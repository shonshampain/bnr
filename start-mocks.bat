@echo off

set VERSION=2.18.0
set PORT=8106

java -jar wiremock/wiremock-standalone-%VERSION%.jar --port %PORT%
