package com.bignerdranch.android.blognerdranch.flows.postlist

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import com.bignerdranch.android.blognerdranch.R
import com.bignerdranch.android.blognerdranch.model.bnr.PostMetadata

class PostListAdapter() : RecyclerView.Adapter<PostViewHolder>() {

    private val differ: AsyncListDiffer<PostMetadata> = AsyncListDiffer(this,
        PostListDiffer()
    )

    fun set(list: List<PostMetadata>) {
        differ.submitList(list)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.post_summary_item, parent, false)
        return PostViewHolder(view)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        val post = differ.currentList[position]
        holder.bind(post)
    }
}