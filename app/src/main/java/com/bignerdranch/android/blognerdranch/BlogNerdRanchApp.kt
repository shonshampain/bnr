package com.bignerdranch.android.blognerdranch

import android.app.Application
import org.koin.android.ext.android.startKoin
import timber.log.Timber

@Suppress("unused") // definitely used
class BlogNerdRanchApp : Application() {

    override fun onCreate() {
        super.onCreate()
        setupTimber()
        startKoin()
    }

    private class CrashReportingTree : Timber.Tree() {
        override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
            // Uncomment this line when access to Crashlytics is hooked up
            // logToCrashlytics(priority, tag, message, t)
        }
    }

    private fun setupTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            Timber.plant(CrashReportingTree())
        }
    }

    private fun startKoin() {
        startKoin(this, listOf(modules))
    }
}