package com.bignerdranch.android.blognerdranch.usecases

import com.bignerdranch.android.blognerdranch.BaseReactiveElement
import com.bignerdranch.android.blognerdranch.model.RxSchedulers
import com.bignerdranch.android.blognerdranch.model.bnr.Post
import com.bignerdranch.android.blognerdranch.repos.PostRepoIF
import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable

interface PostUseCaseIF {
    val postResult: Observable<Post>
    fun getPost(id: Int)
}

class PostUseCase(
    private val postRepo: PostRepoIF,
    override val rxSchedulers: RxSchedulers,
    override val compositeDisposable: CompositeDisposable
) : BaseReactiveElement, PostUseCaseIF {

    private val _postResult: BehaviorRelay<Post> = BehaviorRelay.create()
    override val postResult: Observable<Post> = _postResult

    init {
        react("post search", postRepo.postResult) {
            _postResult.accept(it)
        }

    }
    override fun getPost(id: Int) {
        postRepo.getPost(id)
    }
}