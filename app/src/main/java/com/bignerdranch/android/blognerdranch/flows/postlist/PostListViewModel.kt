package com.bignerdranch.android.blognerdranch.flows.postlist

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bignerdranch.android.blognerdranch.BaseReactiveElement
import com.bignerdranch.android.blognerdranch.LiveEvent
import com.bignerdranch.android.blognerdranch.R
import com.bignerdranch.android.blognerdranch.model.RxSchedulers
import com.bignerdranch.android.blognerdranch.model.bnr.PostMetadata
import com.bignerdranch.android.blognerdranch.usecases.PostListUseCaseIF
import io.reactivex.disposables.CompositeDisposable

data class ViewState(
    val title: (context: Context) -> String = { context -> context.getString(R.string.blog_postings) }
)

sealed class NavEvents {
    data class BNRPostList(val postList: List<PostMetadata>) : NavEvents()
}

class PostListViewModel(
    override val rxSchedulers: RxSchedulers,
    override val compositeDisposable: CompositeDisposable,
    private val postListUseCase: PostListUseCaseIF
) : ViewModel(), BaseReactiveElement {

    private val _navEvents = MutableLiveData<LiveEvent<NavEvents>>()
    val navEvents: LiveData<LiveEvent<NavEvents>> = _navEvents

    private val _viewState = MutableLiveData<ViewState>().apply { value =
        ViewState()
    }
    val viewState: LiveData<ViewState> = _viewState
    private var list: List<PostMetadata>? = null

    init {
        react("get post list", postListUseCase.postListResult) {
            list = it
            _navEvents.value = LiveEvent(NavEvents.BNRPostList(it))
        }
    }

    fun getPostList() {
        if (list == null) {
            postListUseCase.getPostList()
        } else {
            _navEvents.value = LiveEvent(NavEvents.BNRPostList(list!!))
        }
    }
}