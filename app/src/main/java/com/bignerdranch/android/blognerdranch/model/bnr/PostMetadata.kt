package com.bignerdranch.android.blognerdranch.model.bnr

data class PostMetadata(
    val postId: Int = 0,
    var title: String = "",
    var summary: String = "",
    var author: Author = Author(),
    var publishDate: String = ""
)