package com.bignerdranch.android.blognerdranch

import com.bignerdranch.android.blognerdranch.flows.post.PostViewModel
import com.bignerdranch.android.blognerdranch.flows.postlist.PostListViewModel
import com.bignerdranch.android.blognerdranch.model.ApiManager
import com.bignerdranch.android.blognerdranch.model.ApiManagerIF
import com.bignerdranch.android.blognerdranch.model.RxSchedulers
import com.bignerdranch.android.blognerdranch.repos.PostListRepo
import com.bignerdranch.android.blognerdranch.repos.PostListRepoIF
import com.bignerdranch.android.blognerdranch.repos.PostRepo
import com.bignerdranch.android.blognerdranch.repos.PostRepoIF
import com.bignerdranch.android.blognerdranch.usecases.PostListUseCase
import com.bignerdranch.android.blognerdranch.usecases.PostListUseCaseIF
import com.bignerdranch.android.blognerdranch.usecases.PostUseCase
import com.bignerdranch.android.blognerdranch.usecases.PostUseCaseIF
import io.reactivex.disposables.CompositeDisposable
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.Module
import org.koin.dsl.module.module

val modules: Module = module {

    /**
     * Utilities
     */
    single { RxSchedulers() }
    factory { CompositeDisposable() }

    /**
     * Managers
     */
    single { ApiManager() as ApiManagerIF }

    /**
     * Repos
     */
    single { PostRepo(get(), get(), get()) as PostRepoIF }
    single { PostListRepo(get(), get(), get()) as PostListRepoIF }

    /**
     * Use Cases
     */
    single { PostUseCase(get(), get(), get()) as PostUseCaseIF }
    single { PostListUseCase(get(), get(), get()) as PostListUseCaseIF }

    /**
     * ViewModels
     */
    viewModel { PostViewModel(get(), get(), get()) }
    viewModel { PostListViewModel(get(), get(), get()) }
}