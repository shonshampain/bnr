package com.bignerdranch.android.blognerdranch.model

import com.bignerdranch.android.blognerdranch.model.bnr.Post
import com.bignerdranch.android.blognerdranch.model.bnr.PostMetadata
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface BigNerdRanchBlogApiIF {

    @GET("post-metadata")
    fun getPostMetadata(): Single<List<PostMetadata>>

    @GET("post/{id}")
    fun getPost(@Path("id") id: Int): Single<Post>
}