package com.bignerdranch.android.blognerdranch.model.bnr

data class Post(
    val id: Int = 0,
    val metadata: PostMetadata = PostMetadata(),
    val body: String = ""
)