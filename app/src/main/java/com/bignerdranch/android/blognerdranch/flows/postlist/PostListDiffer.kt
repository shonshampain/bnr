package com.bignerdranch.android.blognerdranch.flows.postlist

import androidx.recyclerview.widget.DiffUtil
import com.bignerdranch.android.blognerdranch.model.bnr.PostMetadata

class PostListDiffer : DiffUtil.ItemCallback<PostMetadata>() {

    override fun areItemsTheSame(oldItem: PostMetadata, newItem: PostMetadata): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: PostMetadata, newItem: PostMetadata): Boolean {
        return oldItem == newItem
    }
}