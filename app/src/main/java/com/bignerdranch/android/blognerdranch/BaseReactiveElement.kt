package com.bignerdranch.android.blognerdranch

import com.bignerdranch.android.blognerdranch.model.RxSchedulers
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber

interface BaseReactiveElement {

    val rxSchedulers: RxSchedulers
    val compositeDisposable: CompositeDisposable

    fun <T> react(id: String, o: Observable<T>, onObserved: (T) -> Unit) {
        compositeDisposable += o
            .observeOn(rxSchedulers.observe)
            .subscribeOn(rxSchedulers.subscribe)
            .subscribe({
                onObserved(it)
            },{
                Timber.e(it, "Cannot observe $id")
            })
    }

    fun clear() {
        compositeDisposable.clear()
    }
}