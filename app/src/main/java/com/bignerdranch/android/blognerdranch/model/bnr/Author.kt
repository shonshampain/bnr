package com.bignerdranch.android.blognerdranch.model.bnr

data class Author(
    val name: String = "",
    val imageUrl: String = "",
    val title: String = ""
)