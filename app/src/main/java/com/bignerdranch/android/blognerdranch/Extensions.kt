package com.bignerdranch.android.blognerdranch

import java.text.SimpleDateFormat
import java.util.*

fun String.formatDateTime(): String {
    val inputFormat  = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault())
    val outputFormat = SimpleDateFormat("EEEE MMM dd, yyyy", Locale.getDefault())
    return outputFormat.format(inputFormat.parse(this)!!)
}