package com.bignerdranch.android.blognerdranch.model

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers

data class RxSchedulers(
    val observe: Scheduler = AndroidSchedulers.mainThread(),
    val subscribe: Scheduler = io.reactivex.schedulers.Schedulers.io()
)