package com.bignerdranch.android.blognerdranch.repos

import com.bignerdranch.android.blognerdranch.BaseReactiveElement
import com.bignerdranch.android.blognerdranch.model.ApiManagerIF
import com.bignerdranch.android.blognerdranch.model.RxSchedulers
import com.bignerdranch.android.blognerdranch.model.bnr.PostMetadata
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable

interface PostListRepoIF {
    val postListResult: Observable<List<PostMetadata>>
    fun getPostList()
}

class PostListRepo(
    private val apiManager: ApiManagerIF,
    override val rxSchedulers: RxSchedulers,
    override val compositeDisposable: CompositeDisposable
) : BaseReactiveElement, PostListRepoIF {

    private val _postListResult: PublishRelay<List<PostMetadata>> = PublishRelay.create()
    override val postListResult: Observable<List<PostMetadata>> = _postListResult

    override fun getPostList() {
        react("post search", apiManager.bigNerdRanchBlogApi.getPostMetadata().toObservable()) {
            _postListResult.accept(it)
            clear()
        }
    }
}