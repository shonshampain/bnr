package com.bignerdranch.android.blognerdranch.repos

import com.bignerdranch.android.blognerdranch.BaseReactiveElement
import com.bignerdranch.android.blognerdranch.model.ApiManagerIF
import com.bignerdranch.android.blognerdranch.model.RxSchedulers
import com.bignerdranch.android.blognerdranch.model.bnr.Post
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable

interface PostRepoIF {
    val postResult: Observable<Post>
    fun getPost(id: Int)
}

class PostRepo(
    private val apiManager: ApiManagerIF,
    override val rxSchedulers: RxSchedulers,
    override val compositeDisposable: CompositeDisposable
) : BaseReactiveElement, PostRepoIF {

    private val _postResult: PublishRelay<Post> = PublishRelay.create()
    override val postResult: Observable<Post> = _postResult

    override fun getPost(id: Int) {
        react("post search", apiManager.bigNerdRanchBlogApi.getPost(id).toObservable()) {
            _postResult.accept(it)
            clear()
        }
    }
}