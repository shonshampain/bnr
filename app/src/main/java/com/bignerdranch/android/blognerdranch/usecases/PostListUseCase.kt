package com.bignerdranch.android.blognerdranch.usecases

import com.bignerdranch.android.blognerdranch.BaseReactiveElement
import com.bignerdranch.android.blognerdranch.model.RxSchedulers
import com.bignerdranch.android.blognerdranch.model.bnr.PostMetadata
import com.bignerdranch.android.blognerdranch.repos.PostListRepoIF
import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable

interface PostListUseCaseIF {
    val postListResult: Observable<List<PostMetadata>>
    fun getPostList()
}

class PostListUseCase(
    private val postListRepo: PostListRepoIF,
    override val rxSchedulers: RxSchedulers,
    override val compositeDisposable: CompositeDisposable
) : BaseReactiveElement, PostListUseCaseIF {

    private val _postListResult: BehaviorRelay<List<PostMetadata>> = BehaviorRelay.create()
    override val postListResult: Observable<List<PostMetadata>> = _postListResult

    init {
        react("post search", postListRepo.postListResult) {
            _postListResult.accept(it)
        }

    }

    override fun getPostList() {
        postListRepo.getPostList()
    }
}
