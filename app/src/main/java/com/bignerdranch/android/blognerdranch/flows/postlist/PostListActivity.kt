package com.bignerdranch.android.blognerdranch.flows.postlist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.bignerdranch.android.blognerdranch.ActionObserver
import com.bignerdranch.android.blognerdranch.R
import com.bignerdranch.android.blognerdranch.databinding.ActivityPostListBinding
import org.koin.android.viewmodel.ext.android.viewModel

class PostListActivity : AppCompatActivity() {

    private val vm by viewModel<PostListViewModel>()
    private lateinit var binding: ActivityPostListBinding
    private val postListAdapter = PostListAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupDataBinding()
        setupRecyclerView()
        setupNavigation()
        setupToolbar()

        vm.getPostList()
    }

    private fun setupDataBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_post_list)
        binding.lifecycleOwner = this
        binding.viewState = vm.viewState
    }

    private fun setupRecyclerView() {
        binding.postRecyclerview.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = postListAdapter
        }
    }

    private fun setupNavigation() {
        vm.navEvents.observe(this, ActionObserver {
            when (it) {
                is NavEvents.BNRPostList -> {
                    postListAdapter.set(it.postList)
                }
            }
        })
    }

    private fun setupToolbar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.title = ""
    }
}
