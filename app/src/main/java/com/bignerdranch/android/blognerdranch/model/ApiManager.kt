package com.bignerdranch.android.blognerdranch.model

import com.bignerdranch.android.blognerdranch.BuildConfig
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

private const val READ_TIMEOUT = 30L // seconds
private const val CONNECT_TIMEOUT = 30L // seconds
private const val WRITE_TIMEOUT = 30L // seconds

interface ApiManagerIF {
    val bigNerdRanchBlogApi: BigNerdRanchBlogApiIF
}

class ApiManager : ApiManagerIF {

    // This only works with an emulator as it is effectively localhost
    private val bigNerdRanchApiBaseUrl = "http://10.0.2.2:8106/"
    // To use a live device on your network, enter the server address instead
    // private val bigNerdRanchApiBaseUrl = "http://192.168.0.101:8106/"

    override val bigNerdRanchBlogApi: BigNerdRanchBlogApiIF
        get() = getRetrofit(bigNerdRanchApiBaseUrl).create(BigNerdRanchBlogApiIF::class.java)

    private fun getClient(): OkHttpClient {
        val clientBuilder = OkHttpClient.Builder()
            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
            .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
            .followRedirects(true)
        if (BuildConfig.DEBUG) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            clientBuilder.addInterceptor(interceptor)
        }
        return clientBuilder.build()
    }

    private fun getRetrofit(baseUrl: String): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .client(getClient())
            .build()
    }
}