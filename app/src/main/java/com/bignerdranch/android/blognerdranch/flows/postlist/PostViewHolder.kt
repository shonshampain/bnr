package com.bignerdranch.android.blognerdranch.flows.postlist

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bignerdranch.android.blognerdranch.R
import com.bignerdranch.android.blognerdranch.model.bnr.PostMetadata
import com.bignerdranch.android.blognerdranch.flows.post.PostActivity
import com.bignerdranch.android.blognerdranch.formatDateTime
import java.text.SimpleDateFormat
import java.util.*

class PostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

    private val title: TextView = itemView.findViewById(R.id.post_summary_title)
    private val author: TextView = itemView.findViewById(R.id.post_summary_author)
    private val date: TextView = itemView.findViewById(R.id.post_summary_date)
    private val summary: TextView = itemView.findViewById(R.id.post_summary_summary)
    private var id = 0

    init {
        itemView.setOnClickListener(this)
    }

    fun bind(postMetadata: PostMetadata) {
        val context = itemView.context
        id = postMetadata.postId
        title.text = postMetadata.title
        author.text = context.getString(R.string.author, postMetadata.author.name)
        date.text = context.getString(R.string.published, postMetadata.publishDate.formatDateTime())
        summary.text = postMetadata.summary
    }

    override fun onClick(v: View) {
        val context = v.context
        context.startActivity(PostActivity.newIntent(v.context, id))
        (context as AppCompatActivity).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }
}