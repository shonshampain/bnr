package com.bignerdranch.android.blognerdranch.flows.post

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bignerdranch.android.blognerdranch.BaseReactiveElement
import com.bignerdranch.android.blognerdranch.R
import com.bignerdranch.android.blognerdranch.formatDateTime
import com.bignerdranch.android.blognerdranch.model.RxSchedulers
import com.bignerdranch.android.blognerdranch.usecases.PostUseCaseIF
import io.reactivex.disposables.CompositeDisposable

data class ViewState(
    val id: Int? = null,
    val title: (context: Context) -> String = { context -> context.getString(R.string.blog_post) },
    val postTitle: String = "",
    val author: (context: Context) -> String = { "" },
    val date: (context: Context) -> String = { "" },
    val body: String = ""
)

class PostViewModel(
    override val rxSchedulers: RxSchedulers,
    override val compositeDisposable: CompositeDisposable,
    private val postUseCase: PostUseCaseIF
) : ViewModel(), BaseReactiveElement {

    private val _viewState = MutableLiveData<ViewState>().apply { value =
        ViewState()
    }
    val viewState: LiveData<ViewState> = _viewState

    init {
        react("get post", postUseCase.postResult) {
            _viewState.value = _viewState.value!!.copy(
                id = it.id,
                postTitle = it.metadata.title,
                author = { context -> context.getString(R.string.author, it.metadata.author.name) },
                date = { context -> context.getString(R.string.published, it.metadata.publishDate.formatDateTime()) },
                body = it.body
            )
        }
    }

    fun getPost(id: Int) {
        if (_viewState.value!!.id != id) {
            postUseCase.getPost(id)
        } else {
            _viewState.value = _viewState.value!!
        }
    }
}