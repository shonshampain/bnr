#
# Project Evaluation
# Candidate: Shon Shampain, shon.shampain@gmail.com
#

Step 1: Evaluate the project as if it had been submitted to me by a candidate. Vamos.
-------------------------------------------------------------------------------------
Project structure: seems random, no architecture, no ViewModels, no UseCases, no Repos

build.gradle: sloppy, dependencies are not up to date, target not up to date, many items missing

AndroidManifest.xml: using clear text

*Activity.kt: this is just a disaster
              business logic in the Activity
              findViewById
              failure to properly extract Retrofit
              incorrect (non-Rx) use of Retrofit
              failure to use Timber, littering code with TAGs
              wow! just wow! creating a new adapter each time data is received. wow.
                 
model: failure to use data class
       lot's of nulls                 

Adapter: failure to use AsyncDiffer

BlogService: incorrectly named, this is not a service, it's a Retrofit interface file
(even though you might consider it more or less standard to name it this way,
technically it's incorrect)

No tests

Evaluation: I would rate this project as a complete failure, and not worthy of spending any time on.
If this was submitted to me by a candidate they would fail instantly on the spot.
Approximately equal to a high school student googling "how to program Android" for about an hour.
Recommend complete rewrite.

Program Notes:
--------------
The app provided was tested on a Google Pixel 3a running Android 10,
and an emulator running Android API 28.
No other devices were tested.

Step 2: Let's Re-architect this mess so that we don't waste time fixing bad code. En gard.
------------------------------------------------------------------------------------------
1. Fix build.gradle
2. Create App, plant Timber, switch Log's to Timber's
3. Create proper project package structure
4. Elevate Retrofit to a proper first class citizen
5. Start Koin, create modules, create PostRepo and PostListRepo
6. Create flow VMs, create UCs, move logic out of Activities!!!!
7. Hook up data binding
8. Fix adapter

Step 3: The BNR Challenge, al fin.
----------------------------------
1. Our RecyclerView rows just show the title for each post. It should
   show title, author, summary, and publish date.

SOLUTION:
a. Implement title bar so we give the user some real info
b. Create a PostSummaryItem that uses CardView and some nice layout

#---

2. The UI is bare-bones right now. It should look nicer.

SOLUTION:
a. Extend the title view to the Post page, use up icon here
b. Style this page in a more pleasing sense
c. Create a motion-oriented activity transition

#---

3. The app re-fetches everything from the server across a configuration change.

SOLUTION:
a. Obviously save view state in the VM, that's what it's for

NB: 
------------------------------------------------------------------------------------------
Your mock server connects to localhost
This only works on an emulator, obviously
To run on a live device on your network, you have to enter your proper network address
You should probably mention this in the instructions :-)

#---

4. Archibald's _massive_ blog post takes a long time to download from the server,
   and it's slowing the whole app down. We should improve the user experience for 
   large posts like this.

SOLUTION:
a. I did not have time for this, but here's what I would do:
i) On the fetch of the List<PostMetadata>, I would map this into a list of Ids,
ii) Make a call to pre-fetch each one
iii) And change the PostRepo to maintain a cache, and provide from cache if it existed
iv) Make the PostRepo cache an LRU cache

#---

5. The networking code is duplicated in both of our activities. We should
   clean that up.

SOLUTION:
a. This was done in the refactor, your network layer is now clean and tight

#---

6. There are no tests. It'd be nice to have some tests to verify the quality of
   our work. This could require refactoring the architecture.

SOLUTION:    
(An app refactor.... Really??? You think so??? :-)
a. All the previous 8 refactor commits come into play here.
b. I'm going to link to a Best Practices app that
   I maintain that has both Unit tests and Integration tests w/ Robots. It's cool.
   
#---

Thank you. It's Sunday and I'm done. I hope you enjoy my work.
I look forward to discussing it with you.
I am quite passionate about the fact that Android apps need need NEED to be architected properly.